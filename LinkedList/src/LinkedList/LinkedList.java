package LinkedList;

public class LinkedList<T>{
	private Element<T> start;
	private int size;

	public void add(T element) {
		if (start == null) {
			start = new Element<T>(element);
		} else {
			Element<T> x = start;
			while (x.getNextElement() != null) {
				x = x.getNextElement();
			}
			x.setNextElement(element);
		}
		size++;
	}

	public T get(int index){
		
		Element<T> result = start;
		
		if(index >size && index <0) {
			throw new ArrayIndexOutOfBoundsException();
		}
		
		for (int i = 0; i < index; i++) {
			if (i != index) {
			 result = result.getNextElement();
			} else {
				break;
			}
		}

		return result.getValue();
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("LinkedList [");
		
		for(int i=0; i<size; i++){
			sb.append("(").append(i).append(") ");
			sb.append(get(i).toString());

			if(i<size-1) {
				sb.append(" -> ");
			}
		}
		sb.append(" ]");

		return sb.toString();
	}
	
	
	private class Element<T> {
		private final T value;
		private Element<T> nextElement;

		public Element(T value) {
			super();
			this.value = value;
			this.nextElement = null;
		}

		protected T getValue() {
			return value;
		}

		protected Element<T> getNextElement() {
			return nextElement;
		}

		protected void setNextElement(T element) {
			this.nextElement = new Element<T>(element);
		}

		public String toString() {
			return "Element [value=" + value + "]";
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result;
			result = prime * result + ((value == null) ? 0 : value.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			@SuppressWarnings("unchecked")
			Element<T> other = (Element<T>) obj;

			if (value == null) {
				if (other.value != null)
					return false;
			} else if (!value.equals(other.value))
				return false;
			return true;
		}



		
		
	}

}
