package LinkedList;

public class Person {
	private String name;
	private int pesel;
	
	public Person(String name, int pesel) {
		super();
		this.name = name;
		this.pesel = pesel;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", pesel=" + pesel + "]";
	}

	
	
}
