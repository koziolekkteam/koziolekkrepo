package kata;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidPhoneNumber {

	public static void main(String[] args) {
		System.out.println(validPhoneNumber("(123)456-7890)"));
	}

	public static boolean validPhoneNumber(String phoneNumber) {
		boolean result = false;

		String pattern = "\\(\\d{3}\\) \\d{3}-\\d{4}";

		Pattern p = Pattern.compile(pattern);

		Matcher m = p.matcher(phoneNumber);

		if (m.find()) {
			result = true;
		}

		return result;
	}
}
