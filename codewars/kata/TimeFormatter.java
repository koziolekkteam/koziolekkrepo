package kata;

/**
 * Your task in order to complete this Kata is to write a function which formats a duration, given as a number of seconds, in a human-friendly way.

 The function must accept a non-negative integer. If it is zero, it just returns "now". Otherwise, the duration is expressed as a combination of years, days, hours, minutes and seconds.

 It is much easier to understand with an example:

 formatDuration(62)    // returns "1 minute and 2 seconds"
 formatDuration(3662)  // returns "1 hour, 1 minute and 2 seconds"

 Note that spaces are important.
 Detailed rules

 The resulting expression is made of components like 4 seconds, 1 year, etc. In general, a positive integer and one of the valid units of time, separated by a space. The unit of time is used in plural if the integer is greater than 1.

 The components are separated by a comma and a space (", "). Except the last component, which is separated by " and ", just like it would be written in English.

 A more significant units of time will occur before than a least significant one. Therefore, 1 second and 1 year is not correct, but 1 year and 1 second is.

 Different components have different unit of times. So there is not repeated units like in 5 seconds and 1 second.

 A component will not appear at all if its value happens to be zero. Hence, 1 minute and 0 seconds is not valid, but it should be just 1 minute.

 A unit of time must be used "as much as possible". It means that the function should not return 61 seconds, but 1 minute and 1 second instead. Formally, the duration specified by of a component must not be greater than any valid more significant unit of time.

 For the purpose of this Kata, a year is 365 days and a day is 24 hours.

 */


import java.util.concurrent.TimeUnit;

public class TimeFormatter {
	
	/**
	 *  formatDuration(62)    // returns "1 minute and 2 seconds"
  formatDuration(3662)  // returns "1 hour, 1 minute and 2 seconds
	 */
	
	static final String SEPARATOR = ", ";
	
	public static void main(String[] args) {
		System.out.println(formatDuration(13200012));
	}
	
    public static String formatDuration(int seconds) {
    	int sekundy = 0;
    	int minutes = 0;
    	int hours 	= 0;
    	int days 	= 0;
    	int years 	= 0;
    	
    	if (seconds == 0) {
    		return "now";
    	}
    	else {
    		days 	= (int) TimeUnit.SECONDS.toDays(seconds);
    		hours 	= (int) (TimeUnit.SECONDS.toHours(seconds) - TimeUnit.DAYS.toHours(days));
    		minutes = (int) (TimeUnit.SECONDS.toMinutes(seconds) - TimeUnit.DAYS.toMinutes(days) - TimeUnit.HOURS.toMinutes(hours));
    		sekundy =(int) (TimeUnit.SECONDS.toSeconds(seconds) - TimeUnit.DAYS.toSeconds(days) - TimeUnit.HOURS.toSeconds(hours) - TimeUnit.MINUTES.toSeconds(minutes));
    		years 	= days/365;
    		days 	= days - years *365;   		
    	}
    	    	
		return stringizator(years,days,hours,minutes,sekundy);
    
    }
    
    private static String stringizator(int years, int days, int hours, int minutes, int sekundy) {
    	StringBuilder sb = new StringBuilder();
    	String y = " year";
    	String d = " day";
    	String h = " hour";
    	String m = " minute";
    	String s = " second";

    	int separators = 0;
    	
    	if (years > 0) {
    		sb.append(Integer.toString(years) +  y);
    		
    		if (years > 1) {
    			sb.append("s");
    		}
    	}
    	  	
    	if (days > 0) {
    		if (years>0) {
    			sb.append(SEPARATOR);
    			separators++; //1
    		}
    		
    		sb.append(Integer.toString(days) +  d);
    		
    		if (days > 1) {
    			sb.append("s");
    		}
    	}
    	
    	if (hours > 0) {
    		if ((days>0 || years >0 )&& (separators <= 1)) {
    			sb.append(SEPARATOR);
    			separators++; //1,2
    		}
    		
    		sb.append(Integer.toString(hours) +  h);
    		
    		if (hours > 1) {
    			sb.append("s");
    		}
    	}
    	
    	if (minutes > 0) {
    		if ((years>0 || days>0 || hours > 0) && separators <=2) {
    			sb.append(SEPARATOR);
    			separators++;
    		}
    		
    		sb.append(Integer.toString(minutes) +  m);
    		
    		if (minutes > 1) {
    			sb.append("s");
    		}
    	}
    	
    	if (sekundy > 0) {
    		if ((years>0 || days>0 || hours >0  || minutes>0) && separators <=3) {
    			sb.append(SEPARATOR);
    		}
    		
    		sb.append(Integer.toString(sekundy) +  s);
    		
    		if (sekundy > 1) {
    			sb.append("s");
    		}
    	}

    	return addAnd(sb.toString());
    }

    
    private static String addAnd(String timeWithCommas) {
    	String result = timeWithCommas;
    	int idx = timeWithCommas.lastIndexOf(SEPARATOR);
    	
    	if (idx >-1) {
    		result = timeWithCommas.substring(0, idx) + " and" +timeWithCommas.substring(idx+1);
   	 	}
    	
    	return result;
    }
}
