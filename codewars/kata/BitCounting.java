package kata;

/**
 * Write a function that takes an (unsigned) integer as input, and returns the
 * number of bits that are equal to one in the binary representation of that
 * number.
 * 
 * Example: The binary representation of 1234 is 10011010010, so the function
 * should return 5 in this case
 * 
 * @author Asia
 * 
 */

public class BitCounting {
	
	public static void main(String[] args) {
		System.out.println(countBits(1234));
		System.out.println(countBits2(1234));
	}

	
	public static int countBits(int n) {
		String binaryVal = Integer.toBinaryString(n);
		int result = 0;
		
		System.out.println(binaryVal);
		
		char[] array = binaryVal.toCharArray();
		
		for (int i = 0; i< array.length ; i++) {
			if (array[i] == '1') {
				result++;
			}
		}
		
		return result;		
	}
	
	public static int countBits2(int n) {	
		return Integer.bitCount(n);		
	}
}
