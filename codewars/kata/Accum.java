package kata;

/**
 * This time no story, no theory. The examples below show you how to write function accum:

Examples:

Accumul.accum("abcd");    // "A-Bb-Ccc-Dddd"
Accumul.accum("RqaEzty"); // "R-Qq-Aaa-Eeee-Zzzzz-Tttttt-Yyyyyyy"
Accumul.accum("cwAt");    // "C-Ww-Aaa-Tttt"

The parameter of accum is a string which includes only letters from a..z and A..Z.

 * @author Asia
 *
 */

public class Accum {

	public static void main(String[] args) {
		System.out.println(accum("ZpglnRxqenU"));
	}

	public static String accum(String s) {

		String result = "";
		int max = s.length();

		for (int i = 0; i < max; i++) {

			result += repeat(s.substring(i, i + 1), i + 1);

			if (i != max - 1) {
				result += "-";
			}
		}
		return result;
	}

	private static String repeat(String r, int times) {
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < times; i++) {
			if (i == 0) {
				sb.append(r.toUpperCase());
			} else {
				sb.append(r.toLowerCase());
			}
		}

		return sb.toString();
	}

}
