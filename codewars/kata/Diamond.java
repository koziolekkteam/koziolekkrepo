package kata;

/**
 * This kata is to practice simple string output. Jamie is a programmer, and
 * James' girlfriend. She likes diamonds, and wants a diamond string from James.
 * Since James doesn't know how to make this happen, he needs your help.
 * 
 * ###Task:
 * 
 * You need to return a string that displays a diamond shape on the screen using
 * asterisk ("*") characters. Please see provided test cases for exact output
 * format.
 * 
 * The shape that will be returned from print method resembles a diamond, where
 * the number provided as input represents the number of *�s printed on the
 * middle line. The line above and below will be centered and will have 2 less
 * *�s than the middle line. This reduction by 2 *�s for each line continues
 * until a line with a single * is printed at the top and bottom of the figure.
 * 
 * Return null if input is even number or negative (as it is not possible to
 * print diamond with even number or negative number).
 * 
 * @author Asia
 * 
 */


public class Diamond {
	
	public static void main(String[] args) {

		System.out.println(print(5));
	}
	
	public static String print(int n) {
		String result = "";
		
		int spaces;
		int stars;
		int maxspaces;

		if (n % 2 != 0 && n > 1) {
			maxspaces = n/2;
			spaces = maxspaces;
			stars=1;
				
			for (int i = 0; i< n; i++) {				
				result+= appendAll(spaces, stars);

				spaces--;
				stars = stars+2;
				
				if(stars > n) {
					break;
				}
			}
			
			spaces=0;
			stars= n;
			
			for(int j=0; j<n; j++) {
				spaces++;
				stars = stars-2;
				
				if (stars<1) {
					break;
				}
				result+= appendAll(spaces, stars);
			}
		}
		else {
			result=null;
		}
		
		return result;
	}

	private static String appendAll(int spaces, int stars) {
		StringBuilder sb = new StringBuilder();
				
		for (int a=0; a<spaces; a++) {
			sb.append(" ");
		}
		
		for (int b=0; b<stars; b++) {
			sb.append("*");
		}
		
		sb.append("\n");
		
		return sb.toString();		
	}
	
}
