package hashmap;

import java.util.ArrayList;

public class HashMapA<K,V>  {
	
	private final static double REBUILD_RATIO = 0.2;
	private final static double INCRASE_RATIO = 0.5;
	
	private static int mapSize = 16; //2^4
	private int maxElements = (int) (mapSize * REBUILD_RATIO);
	
	private Object[] table;		
	private int fillFactor = 0;

	public HashMapA() {
		super();
		this.table = new Object[mapSize];
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void put(K key, V value) {
		put(table, key, value);
	}
	
	public void put(Object t[], K key, V value) {
		int idx = hashIt(key);
		
		if (t[idx] == null) {
				t[idx] = new Element(key, value);
				fillFactor ++;
				resizeHashmap();
		} else if (t[idx] instanceof Element) {
            Element<K, V> old = (Element) t[idx];

            if (old.key.equals(key)) {
                t[idx] = new Element(key, value);
            } else {
                ArrayList<Element> row = new ArrayList<Element>();
                row.add(old);
                row.add(new Element(key, value));
                t[idx] = row;
            }
        } else {
            ArrayList<Element> row = (ArrayList<Element>) t[idx];

            for (int i = 0; i < row.size(); i++) {
                if (row.get(i).key.equals(key)) {
                    row.set(i, new Element(key, value));
                    return;
                }
            }

            row.add(new Element(key, value));
        }
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void remove(K key) {
		int idx = hashIt(key);
		
		if (table[idx] != null) {
			if (table[idx] instanceof Element) {
				Element<K, V> e = (Element) table[idx];
				if (e.key.equals(key)) {
					table[idx] = null;
				}
			} else {
				ArrayList<Element> row = (ArrayList<Element>) table[idx];
				for (int i = 0; i < row.size(); i++) {
					if (row.get(i).key.equals(key)) {
							row.set(i, null);
					}
				}
				
				row.remove(null);
				
				 if(row.size() == 1) {
					 table[idx] = row.get(0);					 
				 }else{
					 table[idx] = row;
				 }
			}
		}
		
	}
	
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public V get(K key){
		int idx = hashIt(key);
		Element<K, V> result = null;
		
		if (table[idx] != null) {
            if (table[idx] instanceof Element) {
                Element<K, V> e = (Element) table[idx];
                if (e.key.equals(key)) {
                    result = e;
                }
            } else {
                ArrayList<Element> row = (ArrayList<Element>) table[idx];

                for (int i = 0; i < row.size(); i++) {
                    if (row.get(i).key.equals(key)) {
                        result = row.get(i);
                        break;
                    }
                }
            }
        }
		return result.value;
	}
	

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean containsKey(K key) { 
		return get(key) !=null;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public boolean containsValue(V value) { 
		boolean contains = false;
	
		for (int idx=0; idx< this.table.length; idx++) {

			if(table[idx] == null ) {
				contains = false;
			}
			else if(table[idx] instanceof Element) {
				Element<K, V> e = (Element) table[idx];
				if (e.value.equals(value)) {
					contains = true;
					break;
				}
				
			}
			else {
                ArrayList<Element> row = (ArrayList<Element>) table[idx];
                for (Element e : row) {
                    if (e.value.equals(value)) {
                        contains = true;
                        break;
                    }
                }
            }
		}
		
		return contains;
	}
	
	
	public int getFillFactor() {
		return fillFactor;
	}

	
	private int hashIt(K key) {
		return key.hashCode() % mapSize; // to somehow avoid idx be greater than MAX_MAP_SIZE
	}

	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void resizeHashmap() {
		if (fillFactor >= maxElements){
			int newsize = (int) (fillFactor * mapSize * 2);
		
			Object[] backupTable = table; //backup the table
			Object[] newTable= new Object[newsize];
			table = new Object[newsize]; //resize the old table
			fillFactor = 0; //reset the counter
			mapSize = newsize;
			maxElements = (int) (mapSize * REBUILD_RATIO);
			 
			for (Object o : table) {
				Object row = o;
				
				if (row !=null) {
					if(row instanceof Element) {
						Element e = (Element) row;
						put(newTable, (K) e.key, (V) e.value);
					}
					else {
						ArrayList<Element> list = (ArrayList) row;
						
						for (Element e: list) {
							put(newTable, (K) e.key, (V) e.value);
						}
					}
				}
			 }
			table = newTable;
		}
	}
	
	
	
	

private class Element<K,V> {
	private final K key;
	private final V value;
	
	protected Element(K key, V value) {
		super();
		this.key = key;
		this.value = value;
	}

	@Override
	public String toString() {
		return "Element [key=" + key.toString() + ", value=" + value.toString() + "]";
	}


}

}
