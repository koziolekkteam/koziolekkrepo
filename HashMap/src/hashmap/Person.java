package hashmap;


public class Person {
	private String name;
	private int pesel;
	
	public Person(String name, int pesel) {
		super();
		this.name = name;
		this.pesel = pesel;
	}

	
	
	public String getName() {
		return name;
	}



	public int getPesel() {
		return pesel;
	}



	@Override
	public String toString() {
		return "Person [name=" + name + ", pesel=" + pesel + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + pesel;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (pesel != other.pesel)
			return false;
		return true;
	}

	
	
}
