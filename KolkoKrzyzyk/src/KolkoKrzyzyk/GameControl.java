package KolkoKrzyzyk;

public class GameControl {

	private static final char EMPTYCHAR = '\u0000';
	private static int maxSteps; // maximum possible number of steps

	private Game game;
	private GameDisplay display;
	private char winner;

	public GameControl() {
		super();
		this.game = new Game();
		this.display = new GameDisplay();
		this.winner=EMPTYCHAR;
		GameControl.maxSteps = Game.getMaxSteps();
	}
	
	
	public void printStepInfo() {
		display.displayStep(game.getCurrentStep(), game.isxPlayer());	
	}

	
	public void makeStep(String consoleInput) {
		int field;
		try {
			field = Integer.parseInt(consoleInput);
			saveStep(field, game.isxPlayer());
			togglePlayer();
			printBoard();

		} catch (NumberFormatException e) {
			System.out.println("Enter a valid value!");
		}

	}
	

	public boolean gameIsOver() {
		boolean gameOver = false;

		if (game.getCurrentStep() > 9) {
			return true;
		}
		
		if( whoWon() != EMPTYCHAR) {
			winner =  whoWon();
			return true;
		}

		return gameOver;
	}
	
	private char whoWon() {
		char won = EMPTYCHAR;
		
		char[][] tmpBoard = game.getBoard();
		
		for (int i=0; i<3; i++) { //v
			if (tmpBoard[0][i] == tmpBoard[1][i] && tmpBoard[0][i]  == tmpBoard[2][i]) {
				won = tmpBoard[0][i];
				break;
			}
		}
		
		for (int i=0; i<3; i++) { //h
			if (tmpBoard[i][0] == tmpBoard[i][1] && tmpBoard[i][2]  == tmpBoard[i][1]) {
				won = tmpBoard[i][0];
				break;
			}
		}
		
		if (tmpBoard[0][0] == tmpBoard[1][1] && tmpBoard[0][0] == tmpBoard[2][2] 
			|| tmpBoard[2][0] == tmpBoard[1][1] && tmpBoard[2][0] == tmpBoard[0][0] ){
			won = tmpBoard[1][1];
		}
		

		return won;
	}
	
	
	private boolean saveStep(int fieldNumber, boolean isPlayerX) {
		boolean stepSaved = false;

		int[] rowColumn = mapFieldNoToBoard(fieldNumber);
		int row = rowColumn[0];
		int column = rowColumn[1];
		
		int currentStep = game.getCurrentStep();

		if (fieldNumber > 0 && fieldNumber <= maxSteps
				&& currentStep <= maxSteps) { // user entered valid field number and step does not exceed 9
			char[][] tmpBoard = game.getBoard();

			if (tmpBoard[row][column] == EMPTYCHAR) { // save only when the field is empty
				tmpBoard[row][column]= isPlayerX ?'x' : 'o';
				game.setBoard(tmpBoard);

				game.setCurrentStep(currentStep + 1);
				stepSaved = true;
			}
			else {
				System.out.println("The field " + Integer.toString(fieldNumber) +  " is already in use!");
			}
		}

		return stepSaved;
	}
	
	private int[] mapFieldNoToBoard (int fieldNumber) {
		int[] rowColumn = new int[2];
		
		if (fieldNumber == 1){
			rowColumn=new int[] {0, 0};			
		}else if (fieldNumber == 2) {
			rowColumn=new int[] {0, 1};
		}else if (fieldNumber == 3) {
			rowColumn=new int[] {0, 2};
		}else if (fieldNumber == 4) {
			rowColumn=new int[] {1, 0};
		}else if (fieldNumber == 5) {
			rowColumn=new int[] {1, 1};
		}else if (fieldNumber == 6) {
			rowColumn=new int[] {1, 2};
		}else if (fieldNumber == 7) {
			rowColumn=new int[] {2, 0};
		}else if (fieldNumber == 8) {
			rowColumn=new int[] {2, 1};
		}else {
			rowColumn=new int[] {2, 2};
		}
		
		return rowColumn ;
	
	}

	private void togglePlayer() {
		if (game.getCurrentStep() % 2 != 0) { // X playing
			game.setxPlayer(true);

		} else { // O playing
			game.setxPlayer(false);
		}
	}
		
	private void printBoard() {
		display.displayBoard(game.getBoard());	
	}


	public char getWinner() {
		return winner;
	}

}
