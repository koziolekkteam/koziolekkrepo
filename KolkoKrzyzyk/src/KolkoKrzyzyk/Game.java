package KolkoKrzyzyk;

public class Game {

	private static final int MAX_STEPS = 9; // maximum possible number of steps
	private static final int MAX_SIZE = 3; // maximum board size
	
	private char[][] board; // game board; if element == null -> nobody played, if 0 -> O, if 1 -> X;

	private boolean gameFinished; // Is game finished (max steps reached or somebody won)
	private boolean xPlayer; // If true -> X is playing, else -> O is playing

	private int currentStep; // 1<= step <=9

	public Game() {
		super();
		this.board = new char[MAX_SIZE][MAX_SIZE];
		this.gameFinished = false;
		this.xPlayer = true;
		this.currentStep = 1;
	}

	public boolean isGameFinished() {
		return gameFinished;
	}

	public void setGameFinished(boolean gameFinished) {
		this.gameFinished = gameFinished;
	}

	public boolean isxPlayer() {
		return xPlayer;
	}

	public void setxPlayer(boolean xPlayer) {
		this.xPlayer = xPlayer;
	}

	public int getCurrentStep() {
		return currentStep;
	}

	public void setCurrentStep(int currentStep) {
		this.currentStep = currentStep;
	}

	public char[][] getBoard() {
		return board;
	}

	public void setBoard(char[][] board) {
		this.board = board;
	}

	public static int getMaxSteps() {
		return MAX_STEPS;
	}

}
