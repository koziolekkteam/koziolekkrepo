package KolkoKrzyzyk;

import java.util.Scanner;

public class KolkoKrzyzykApp {

	private static Scanner scanner = new Scanner(System.in);
	private static GameControl gameControl = new GameControl();

	public static void main(String[] args) {
		System.out.println("K�ko i krzy�yk");
		play();
		System.out.print("\nThe end!!!!!!!!");
	}

	private static void play() {
		while (!gameControl.gameIsOver()) {
			gameControl.printStepInfo();
			gameControl.makeStep(scanner.nextLine());
		}
		
		if(gameControl.getWinner() != '\u0000') {
			System.out.print("Player won: " + gameControl.getWinner());
		}
		
		scanner.close();
	}

}
