package KolkoKrzyzyk;

public class GameDisplay {
	
	private static final String TEXT0 = "\nStep ";
	private static final String TEXT1 = "\nEnter field number to put %s: ";
	
	public void displayStep(int stepNo, boolean isXplayer) {
		System.out.println(TEXT0 + Integer.toString(stepNo));
		System.out.print(String.format(TEXT1, isXplayer ? "X" : "O"));		
	}
	
	public void displayBoard(char[][] board) {
		
		System.out.println(board[0][0] + "|" + board [0][1] + "|" + board[0][2]);
		System.out.println("-----");
		System.out.println(board[1][0] + "|" + board [1][1] + "|" + board[1][2]);
		System.out.println("-----");
		System.out.println(board[2][0] + "|" + board[2][1] + "|" + board[2][2]);
		System.out.println("____________________________________");
		
	}
	
	/**
	 * x|x|x
	 * -----
	 * o|o|o
	 * -----
	 * x|x|x
	 * 
	 */

}
