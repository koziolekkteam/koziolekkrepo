package xmaswars;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CountDistinctWordsInFileApp {
	
	public static void main(String[] args) {
		String filePath = "src/xmaswars/testFiles/test1.txt";
		
		System.out.println("Count distinct words in a file." + filePath);
		   
		Map<String, Integer> result = findDistinctWords(filePath);
		
		displayTopN(result, 3);		
	}
	
	public static 	Map<String, Integer> findDistinctWords(String filePath) {
		Map<String, Integer> distinctWords = new HashMap<String, Integer>();
		
		try {
			if (filePath != null && !filePath.isEmpty()) {
				
				FileReader fileReader = new FileReader(new File(filePath));	
				BufferedReader br = new BufferedReader(fileReader);
			
				String line = br.readLine();
				
				while (line != null) {
					
					if (line.length() > 0) {
						String linewords[] = line.split("[\\s.,?!]+"); //take words without white sings and punctuation
						
						for(int i=0; i<linewords.length; i++) {
							String word = linewords[i].toLowerCase(); //just not to duplicate small/big letter words, but loosing the info :(
							
							if (word.matches("[a-z]+")) { //letter-words only (no numbers)
								if (distinctWords.containsKey(word)) { //if already found, just increment the counter
									distinctWords.put(word, distinctWords.get(word) + 1);
								}
								else { //put a new word
									distinctWords.put(word, 1);
								}
							}
						
						}
					}
					line = br.readLine();
				}
				
				
				fileReader.close();
				br.close();
			}						
			
		} catch (FileNotFoundException e) {	
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		return distinctWords;
	
	}
	
	
	/**
	 * https://stackoverflow.com/questions/15436516/get-top-10-values-in-hash-map
	 * @param unsortedMap
	 * @param n
	 * @return
	 */
	private static List<Entry<String,Integer>> selectTopN(Map<String, Integer> unsortedMap, int n) {
		
		List<Entry<String,Integer>> topN = new ArrayList<>(unsortedMap.entrySet());		
		Collections.sort(topN, new Comparator<Entry<String, Integer>>() {
			@Override
			public int compare(Entry<String, Integer> e1, Entry<String, Integer> e2) {
				if (e1.getValue() < e2.getValue()) {
		            return 1;
		        } else if (e1.getValue() > e2.getValue()) {
		            return -1;
		        }
		        return 0;
			}
			
		});
		
		topN =  topN.subList(0, n);
		return topN;		
	}
	
	private static void displayTopN(Map<String, Integer> unsortedMap, int n) {
		
		List<Entry<String, Integer>> resultTopN = selectTopN(unsortedMap, n);
		
		System.out.println("Top " + n + " elements:");		   
		for (Entry<String, Integer> element : resultTopN) {
			System.out.println(element.getKey() + " - count: " + element.getValue() );
		
		}
		
	}
}

